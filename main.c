#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "linked_list.h"

#define BUFFER_SIZE 500

bool check_exit(char* input){
	return !(strcmp(input, "quit\n") && strcmp(input, "q\n") && strcmp(input, "quit") && strcmp(input, "q"));
}

void std_out_spaced(int d) {
    printf("%d ", d);
}

void std_out_newline(int d) {
    printf("%d\n", d);
}

int square(int d) {
    return d * d;
}

int cube(int d) {
    return d * d * d;
}

int sum(int a, int d) {
    a = a + d;
    return a;
}

int max(int a, int d) {
    if (d > a) {
        a = d;
    }
    return a;
}

int min(int a, int d) {
    if (d < a) {
        a = d;
    }
    return a;
}

int abs(int a) {
    if (a < 0) return -a;
    return a;
}

int x2(int d) {
    return d*2;
}

int main(){
	char input[BUFFER_SIZE];
	puts("Enter integer numbers separated by «SPACE» character");
	fgets(input, BUFFER_SIZE, stdin);
	if(check_exit(input)) return 0;
	int* elements = read_ints(input);
	struct linked_list* list = create_list_from_array(elements, 0);
    puts("Stdout output");
	foreach(list, std_out_spaced);
    puts("\nAnd now on with a different separator");
    foreach(list, std_out_newline);
    puts("Squares");
    print_list(map(list, square));
    puts("Cubes");
    print_list(map(list, cube));
    printf("Sum: %d\n", foldl(0, list, sum));
    printf("Max: %d\n", foldl(INT_MIN, list, max));
    printf("Min: %d\n", foldl(INT_MAX, list, min));
    puts("Absolute values");
    print_list(map_mut(list, abs));
    puts("Powers of 2");
    print_list(iterate(1, 10, x2));
    save(list, "list.txt");
    struct linked_list *read_list = create_list();
    struct linked_list **pread_list = &read_list;
    load(pread_list, "list.txt");
    puts("Read list");
    print_list(read_list);
    puts("Deserialized list");
    serialize(list, "list.bin");
    struct linked_list *deserialized_list = create_list();
    struct linked_list **pdeserialized_list = &deserialized_list;
    deserialize(pdeserialized_list, "list.bin");
    print_list(deserialized_list);
    list_free(deserialized_list);
    list_free(read_list);
    list_free(list);
}