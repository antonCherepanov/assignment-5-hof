#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <stdbool.h>
#include <sys/stat.h>
#include "linked_list.h"

#define BUFFER_SIZE 500

struct node {
    int value;
    struct node* previous;
    struct node* next;
};


struct linked_list {
    struct node* first;
    struct node* last;
    int size;
};

static size_t elements_number;
static char buffer[BUFFER_SIZE];
static int int_buffer[BUFFER_SIZE];

struct node* create_node(int value) {
    struct node* res = malloc(sizeof(struct node));
    res->value = value;
    res->next = NULL;
    res->previous = NULL;
    return res;
}

struct linked_list* create_list(){
	struct linked_list* list = malloc(sizeof(struct linked_list));
	list->first = NULL;
	list->last = NULL;
	list->size = 0;
	return list;
}

struct linked_list* create_list_from_array(int* array, size_t size){
	size_t real_size = size;
	if(size == 0) real_size = elements_number;
	struct linked_list* list = malloc(sizeof(struct linked_list));
	for (size_t i = 0; i < real_size; i++) {
        list_add_back(list, array[i]);
    }
    return list; 
} 

void list_free(struct linked_list* list){
	struct node* current_node = list->first;
   	struct node* next_node;
    while (current_node) {
        next_node = current_node->next;
    	free(current_node);
    	current_node = next_node;
    };
	free(list);
}

int* read_ints(char *string) {
    int* array = malloc(sizeof(int) * 100);
    size_t i = 0;
    size_t j = 0;
    size_t length = strlen(string);
    int current = 0;
    bool is_negative = false;
    elements_number = 0;
    while (j < length) {
        if (string[j] == '-') {
            is_negative = true;
        } else if (string[j] >= '0' && string[j] <= '9') {
            current *= 10;
            current += string[j] - 48;
        } else {
            if (is_negative) {
                current = -current;
                is_negative = false;
            }
            array[i++] = current;
            elements_number++;
            current = 0;
        }
        j++;
    }
    return array;
}

int list_length(struct linked_list* list){
	return list->size;
}

struct node* list_node_at(struct linked_list* list, int index){
	int size = list->size; 
	struct node* element;
	if(size <= index || index < 0) return NULL;
	if(index <= size/2){
		element = list->first;
		for(int i = 0; i < index; i++){
			element=element->next;
		}
	}else{
		element = list->last;
		for(int i = size-1; i > index; i--){
			element=element->previous;
		}
	}
	return element;
}

long list_sum(struct linked_list* list){
	int size = list->size; 
	struct node* element = list->first;
	long sum = 0L;
	for(int i = 0; i < size; i++){
		sum+=element->value;
		element=element->next;
	}
	return sum;
}


int list_get(struct linked_list* list, int index){
	struct node* element = list_node_at(list, index);
	if(element == NULL) return 0;
	return element->value;
}

void list_add_front(struct linked_list* list, int value){
	struct node* element = create_node(value);
	struct node* first = list->first;
	element->next = first;
	if(list->size == 0){
		list->last = element;
	}else{
		first->previous = element;
	}
	list->first = element;
	list->size++;
}

void list_add_back(struct linked_list* list, int value){
	struct node* element = create_node(value);
	struct node* last = list->last;
	element->previous = last;
	if(list->size == 0){
		list->first = element;
	}else{
		last->next = element;
	}
	list->last = element;
	list->size++;
}

void foreach(struct linked_list* list, void (*fptr)(int)) {
    struct node* current_node = list->first;
    for (int i = 0; i < list->size; i++) {
        fptr(current_node->value);
        current_node = current_node->next;
    }
}

struct linked_list* map(struct linked_list* source_list, int (*fptr)(int)) {
    struct linked_list* result_list = create_list();
    struct node* current_node = source_list->first;
    for (int i = 0; i < source_list->size; i++) {
        list_add_back(result_list, fptr(current_node->value));
        current_node = current_node->next;
    }
    return result_list;
}

struct linked_list* map_mut(struct linked_list* list, int (*fptr)(int)) {
    struct node* current_node = list->first;
    for (int i = 0; i < list->size; i++) {
        current_node->value = fptr(current_node->value);
        current_node = current_node->next;
    }
    return list;
}

int foldl(int accumulator, struct linked_list* list, int (*fptr)(int, int)) {
    struct node* current_node = list->first;
    for (int i = 0; i < list->size; i++) {
        accumulator = fptr(accumulator, current_node->value);
        current_node = current_node->next;
    }
    return accumulator;
}

struct linked_list* iterate(const int initial, const int length, const int (*fptr)(int)) {
	int value = initial;
    struct linked_list* list = create_list();
    for (int i = 0; i < length; ++i) {
        list_add_back(list, value);
        value = fptr(value);
    }
    return list;
}

void print_list(struct linked_list* list) {
    struct node* current_node = list->first;
    for (int i = 0; i < list->size; ++i) {
        printf("list[%d]: %d\n", i, current_node->value);
        if (current_node->next) {
            current_node = current_node->next;
        }
    }
}

void add_to_str(int d) {
    char s[10];
    sprintf(s, "%d ", d);
    strcat(buffer, s);
}

bool save(struct linked_list* list, const char* filename) {
    foreach(list, (void (*)(int)) add_to_str);
    FILE* file = fopen(filename, "w");
    size_t result = fwrite(buffer, sizeof(char), strlen(buffer), file);
    fclose(file);
    return result >= 0;
}

bool load(struct linked_list** list, const char* filename) {
    FILE* file = fopen(filename, "r");
    fgets(buffer, BUFFER_SIZE, file);
    int* array = read_ints(buffer);
    struct linked_list* result = create_list_from_array(array, elements_number);
    (*list)->first = result->first;
    (*list)->size = result->size;
    (*list)->last = result->last;
    fclose(file);
    free(result);
    return true;
}

bool serialize(struct linked_list* list, const char* filename) {
    struct node* current_node = list->first;
    for (int i = 0; i < list->size; i++) {
        int_buffer[i] = current_node->value;
        current_node = current_node->next;
    }
    FILE* file = fopen(filename, "wb");
    size_t result = fwrite(int_buffer, sizeof(int), list->size, file);
    fclose(file);
    return result >= 0;
}

bool deserialize(struct linked_list** list, const char* filename) {
    FILE* file = fopen(filename, "rb");
    struct stat file_stat;
    fstat(fileno(file), &file_stat);
    size_t size = file_stat.st_size / sizeof(int);
    fread(int_buffer, sizeof(int), size, file);
    struct linked_list* result = create_list_from_array(int_buffer, size);
    (*list)->first = result->first;
    (*list)->size = result->size;
    (*list)->last = result->last;
    fclose(file);
    free(result);
    return true;
}