#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

struct node;

struct linked_list;

struct node *create_node(int value);

struct linked_list* create_list();

struct linked_list* create_list_from_array(int* array, size_t size);

void list_free(struct linked_list* list);

int* read_ints(char *string);

int list_length(struct linked_list* list);

struct node* list_node_at(struct linked_list* list, int index);

long list_sum(struct linked_list* list);

int list_get(struct linked_list* list, int index);

void list_add_front(struct linked_list* list, int value);

void list_add_back(struct linked_list* list, int value);

void foreach(struct linked_list* list, void (*fptr)(int));

struct linked_list* map(struct linked_list* source_list, int (*fptr)(int));

struct linked_list* map_mut(struct linked_list* list, int (*fptr)(int));

int foldl(int accumulator, struct linked_list* list, int (*fptr)(int, int));

struct linked_list* iterate(const int initial, const int length, const int (*fptr)(int));

void print_list(struct linked_list* list);

bool save(struct linked_list* list, const char* filename);

bool load(struct linked_list** list, const char* filename);

bool serialize(struct linked_list* list, const char* filename);

bool deserialize(struct linked_list** list, const char* filename);

#endif